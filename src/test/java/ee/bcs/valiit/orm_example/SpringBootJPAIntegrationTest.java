package ee.bcs.valiit.orm_example;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ee.bcs.valiit.orm_example.domain.Address;
import ee.bcs.valiit.orm_example.domain.Person;
import ee.bcs.valiit.orm_example.repository.AddressRepository;
import ee.bcs.valiit.orm_example.repository.PersonRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class SpringBootJPAIntegrationTest {
  
    @Autowired
    private AddressRepository addressRepository;
    
    @Autowired
    private PersonRepository personRepository;
 
    @Test
    public void insertNewAddressAndVerifySave() {
    	Address a = new Address();
    	a.setCountry("eesti");
    	
        Address address = addressRepository.save(a);
        Address foundEntity = addressRepository.findOne(address.getId());
  
        assertNotNull(foundEntity);
        assertEquals(a.getCountry(), foundEntity.getCountry());
        
        addressRepository.delete(address.getId());
    }
    
    @Test
    public void insertNewPersonAndVerifySave() {
    	Person p = new Person();
    	p.setFirstName("Jaan");
    	
    	Person person = personRepository.save(p);
    	Person foundEntity = personRepository.findOne(person.getId());
    	assertNotNull(foundEntity);
    	assertEquals(p.getFirstName(), foundEntity.getFirstName());
    	
    	personRepository.delete(person.getId());
    }
}