/*Create sample data*/
WITH person1 AS (
	INSERT INTO person(id, modified_by, first_name, last_name) VALUES (DEFAULT,'System', 'Jaan', 'Kama') RETURNING id
)
INSERT INTO address(person_id, modified_by, address, town, postal_code, country) SELECT id, 'System', 'Jaama 8', 'Tartu', '11022', 'Eesti' FROM person1;

WITH person2 AS (
	INSERT INTO person(id, modified_by, first_name, last_name) VALUES (DEFAULT, 'System', 'Peeter', 'Kelk') RETURNING id
)
INSERT INTO address(person_id, modified_by, address, town, postal_code, country) SELECT id, 'System', 'Viru 3', 'Tallinn', '56666', 'Eesti' FROM person2;
