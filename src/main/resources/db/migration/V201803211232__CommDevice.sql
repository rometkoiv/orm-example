/* Drop Tables */
DROP TABLE IF EXISTS "comm_device" CASCADE;

/* Drop Sequences */
DROP SEQUENCE IF EXISTS "comm_device_id_seq";


/* Create Sequences*/
CREATE SEQUENCE "comm_device_id_seq" START 1 INCREMENT 1;


/* Create Tables */
CREATE TABLE "comm_device" (
	"id" bigint NOT NULL PRIMARY KEY DEFAULT nextval('comm_device_id_seq'::REGCLASS),
	"person_id" bigint,
	"modified_date" TIMESTAMP,
	"modified_by" varchar(50),
	"name" varchar(150),
	"type" varchar(10)
);


/* Relate tables, after delete commdevice will be removed, update will cascade */
ALTER TABLE "comm_device" ADD CONSTRAINT "FK_comm_device_person" FOREIGN KEY ("person_id") REFERENCES "person" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

/* Add Table Comments */

COMMENT ON TABLE "comm_device" IS 'Communication devices';
COMMENT ON COLUMN "comm_device"."name" IS 'Freetext of device';
COMMENT ON COLUMN "comm_device"."type" IS 'Device type';

