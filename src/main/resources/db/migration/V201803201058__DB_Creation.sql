/* Drop Tables */
DROP TABLE IF EXISTS "person" CASCADE;
DROP TABLE IF EXISTS "address" CASCADE;

/* Drop Sequences */
DROP SEQUENCE IF EXISTS "person_id_seq";
DROP SEQUENCE IF EXISTS "address_id_seq";

/* Create Sequences*/
CREATE SEQUENCE "person_id_seq" START 1 INCREMENT 1;
CREATE SEQUENCE "address_id_seq" START 1 INCREMENT 1;

/* Create Tables */
CREATE TABLE "person" (
	"id" bigint NOT NULL PRIMARY KEY DEFAULT nextval('person_id_seq'::REGCLASS),
	"modified_date" TIMESTAMP,
	"modified_by" varchar(50),
	"first_name" varchar(50),
	"last_name" varchar(150)
);

CREATE TABLE "address" (
	"id" bigint NOT NULL PRIMARY KEY DEFAULT nextval('address_id_seq'::REGCLASS),
	"person_id" bigint,
	"modified_date" TIMESTAMP,
	"modified_by" varchar(50),
	"address" varchar(500),
	"town" varchar(100),
	"postal_code" varchar(50),
	"country" varchar(50)
);

/* Relate tables, delete remains address, update will cascade */
ALTER TABLE "address" ADD CONSTRAINT "FK_address_person" FOREIGN KEY ("person_id") REFERENCES "person" ("id") ON DELETE No Action ON UPDATE CASCADE;

/* Add Table Comments */

COMMENT ON TABLE "person" IS 'Person';
COMMENT ON COLUMN "person"."first_name" IS 'Firstname';
COMMENT ON COLUMN "person"."last_name" IS 'Lastname';

COMMENT ON TABLE "address" IS 'Address';
COMMENT ON COLUMN "address"."address" IS 'Freetext of address';
COMMENT ON COLUMN "address"."town" IS 'Town name';
COMMENT ON COLUMN "address"."postal_code" IS 'Postal Code';
COMMENT ON COLUMN "address"."country" IS 'Country name';

