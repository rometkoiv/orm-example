package ee.bcs.valiit.orm_example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import ee.bcs.valiit.orm_example.domain.CommunicationDevice;

public interface CommunicationDeviceRepository extends CrudRepository<CommunicationDevice, Long>{
	@Query("from CommunicationDevice c where c.type=:type")
	public List<CommunicationDevice> getByType(@Param("type") String type);

}
