package ee.bcs.valiit.orm_example.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ee.bcs.valiit.orm_example.domain.Person;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {}
