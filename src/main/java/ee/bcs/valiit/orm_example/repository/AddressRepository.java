package ee.bcs.valiit.orm_example.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ee.bcs.valiit.orm_example.domain.Address;

@Repository
public interface AddressRepository extends CrudRepository<Address, Long> {
	
	@Query(
		value = "update address set person_id = :personId where id= :id RETURNING *",
		nativeQuery = true
	)
	Address linkPerson(@Param("id") Long id, @Param("personId") Long personId);
}
