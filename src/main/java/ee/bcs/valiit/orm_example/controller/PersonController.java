package ee.bcs.valiit.orm_example.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ee.bcs.valiit.orm_example.domain.Person;
import ee.bcs.valiit.orm_example.repository.PersonRepository;

@RestController
@RequestMapping("/Person")
public class PersonController {
	@Autowired
	private PersonRepository personRepository;
	
	@GetMapping("/list")
	public Iterable<Person> getPersons() {
		return personRepository.findAll();
	}
	
	@PostMapping
	public Person updatePerson(Person person) {
		return personRepository.save(person);
	}
	
	@GetMapping("/delete/{id}")
	public String removePerson(@PathVariable Optional<String> id) {
		if (id.isPresent()) {
			personRepository.delete(Long.valueOf(id.get()));
			return "SUCCESS";
		} else {
			return "ID is missing";
		}
	}
}
