package ee.bcs.valiit.orm_example.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ee.bcs.valiit.orm_example.domain.Address;
import ee.bcs.valiit.orm_example.repository.AddressRepository;


@RestController
@RequestMapping("/Address")
public class AddressController {
	@Autowired
	private AddressRepository addressRepository;
	
	@GetMapping("/list")
	public Iterable<Address> getAddress() {
		return addressRepository.findAll();
	}
	
	@PostMapping
	public Address updateAddress(Address address) {
		return addressRepository.save(address);
	}
	
	@GetMapping("/delete/{id}")
	public String removeAdress(@PathVariable Optional<String> id) {
		if (id.isPresent()) {
			addressRepository.delete(Long.valueOf(id.get()));
			return "SUCCESS";
		} else {
			return "ID is missing";
		}
	}
	
	@GetMapping("/link/{id}/person/{personId}")
	public Address linkAddress(@PathVariable Optional<String> id, @PathVariable Optional<String> personId) {
		if (id.isPresent() && personId.isPresent()) {
			Address address = addressRepository.linkPerson(Long.valueOf(id.get()), Long.valueOf(personId.get()));
			return addressRepository.save(address);
		} else  if (id.isPresent()){
			return addressRepository.findOne(Long.valueOf(id.get()));
		}
		return null;
	}
}
