package ee.bcs.valiit.orm_example.controller;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ee.bcs.valiit.orm_example.domain.CommunicationDevice;
import ee.bcs.valiit.orm_example.repository.CommunicationDeviceRepository;

@RestController
@RequestMapping("/commDevice")
public class CommDeviceController {
	
	@Autowired
	private CommunicationDeviceRepository communicationDeviceRepository;
	
	@GetMapping("/filter/{type}")
	public List<CommunicationDevice> filterDevice (@PathVariable Optional<String> type) {
		if (type.isPresent()) return communicationDeviceRepository.getByType(type.get());
		return new LinkedList<CommunicationDevice>();
	}
	
}
