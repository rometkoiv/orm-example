package ee.bcs.valiit.orm_example.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "address")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Address extends BaseTimestampEntity{

	private String modifiedBy;
	@Column(name="address")
	private String street;
	private String town;
	private String postalCode;
	private String country;
	
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String address) {
		this.street = address;
	}
	public String getTown() {
		return town;
	}
	public void setTown(String town) {
		this.town = town;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postal_code) {
		this.postalCode = postal_code;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	

}
