package ee.bcs.valiit.orm_example.domain;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="person")
public class Person extends BaseTimestampEntity{
	
	private String modifiedBy;
	private String firstName;
	private String lastName;
	
		
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "person_id")
	private List<Address> addresses;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "person_id")
	private List<CommunicationDevice> communicationDevice;
	
	
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public List<Address> getAddresses() {
		return addresses;
	}
	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}
	
	public void addAddress(Address address) {
		if (getAddresses() == null) setAddresses(new LinkedList<Address>());
		getAddresses().add(address);
	}
	public List<CommunicationDevice> getCommunicationDevice() {
		return communicationDevice;
	}
	public void setCommunicationDevice(List<CommunicationDevice> communicationDevice) {
		this.communicationDevice = communicationDevice;
	}
	
}
