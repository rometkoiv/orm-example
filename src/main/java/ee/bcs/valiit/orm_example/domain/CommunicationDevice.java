package ee.bcs.valiit.orm_example.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="comm_device")
public class CommunicationDevice extends BaseTimestampEntity{
	
	private String name;
	private String type;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
