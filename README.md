# ORM Example #

## Gradle project ##
### Eclipse ###

- File > import > Existing Gradle Project. If no local gradle setup, use wrapper
- Project can be run as Java application. Run > run configuration > Java Application: Main class ee.bcs.valiit.orm_example.Application, project orm-example
- Run > run configuration > Gradle Project: (task name is) bootRun

### Command Prompt ###
- If gradle exists gradle bootRun
- Else .\gradlew bootRun

### Commandline jar creation ###
- .\gradlew bootRepackage
- java -jar build\libs\orm-example-1.0.jar

Requires Postgre SQL database